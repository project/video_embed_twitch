CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Features
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Video Embed Twitch extends the Video Embed Field that allows you
to embed Twitch Channels as an iframe simply by entering the channel url.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/video_embed_twitch

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/video_embed_twitch


REQUIREMENTS
------------

This module requires the following modules:

 * Video Embed Field (https://www.drupal.org/project/video_embed_field)


FEATURES
--------

This module enables you to add Twitch iframes:

 * Add Twitch Channels
 * Add Twitch Clips
 * Add Twitch Collections
 * Add Twitch Video

 Since june 2020 you need to use https on your domain in order to use Twitch embeds via iframe.
 Else Content Security Policy will block the iframe from loading. So testing locally without https will not work.
 It will work using “localhost” or “127.0.0.1” as your local domain value in the parent parameter iframe url.

 See: https://discuss.dev.twitch.tv/t/twitch-embedded-player-updates-in-2020/23956/117

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Go to "Structure" -> "Media types".
 * Add a media type and choose "Video Embed Field" as a Media source.
 * Add a media field or edit an existing media field.
 * Enable Twitch providers.

You can now paste Twitch urls when adding or editing media.

MAINTAINERS
-----------

Current maintainers:
 * Rick Verwoerd (fdverwoerd) - https://www.drupal.org/u/fdverwoerd

This project has been sponsored by:
 * YOP Online
   YOP Online is a Dutch online gaming community or also
   known as a clan. Since 2002 we play on the Sony Playstation console.
   Our website is build with Drupal and we love it.
