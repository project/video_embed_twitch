<?php

namespace Drupal\video_embed_twitch\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a Twitch Clip plugin for video_embed_field.
 *
 * @VideoEmbedProvider(
 *   id = "twitch_clip",
 *   title = @Translation("Twitch Clip")
 * )
 */
class TwitchClip extends ProviderPluginBase {

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Twitch Constructor.
   *
   * @param array $configuration
   *   The configuration of the plugin.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *    An HTTP client.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *    The request stack.
   *
   * @throws \Exception
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, ClientInterface $http_client, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client);
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('http_client'), $container->get('request_stack'));
  }

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    return [
      '#type' => 'video_embed_iframe',
      '#provider' => 'twitch_clip',
      '#url' => sprintf('https://clips.twitch.tv/embed?clip=%s&parent=%s', $this->getVideoId(), $this->request->getHost()),
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
        'scrolling' => 'no',
        'allowfullscreen' => 'allowfullscreen',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    $image_url = $this->twitchGetThumbnailUrl($this->getVideoId());
    if ($image_url) {
      return $image_url;
    }
    return FALSE;
  }

  /**
   * Helper function to get a thumbnail for Twitch Clip.
   *
   * Crawls the clip url looking for og:image meta element.
   *
   * @param string $id
   *   The Twitch clip slug.
   *
   * @return bool|array
   *   Returns FALSE when no image found or array containing image url.
   */
  public static function twitchGetThumbnailUrl($id) {
    $matches = [];
    $url = 'https://clips.twitch.tv/' . $id;
    $client = \Drupal::httpClient();

    $response = $client->get($url);
    $response_body = $response->getBody()->getContents();
    if (!empty($response_body)) {
      // Get image from Open Graph metatag:
      // <meta property='og:image' content='.
      preg_match("/<meta property='og:image' content='(.*?)'>/", $response_body, $matches);
    }
    else {
      return FALSE;
    }
    if (!empty($matches[1])) {
      return $matches[1];
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    preg_match('/^https?:\/\/clips\.twitch\.tv\/(?<slug>[a-zA-Z0-9_-]*)?$/', $input, $matches);
    if (!isset($matches['slug'])) {
      preg_match('/^https?:\/\/(www\.)?twitch\.tv\/(?<user>[a-zA-Z0-9_-]*)\/clip\/(?<slug>[a-zA-Z0-9_-]*)?$/', $input, $matches);
      return isset($matches['slug']) ? $matches['slug'] : FALSE;
    }
    else {
      return isset($matches['slug']) ? $matches['slug'] : FALSE;
    }
  }

}
